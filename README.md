<html>
<body>
<h2>Privacy Policy</h2>
<p>Our team built the app as a free app.</p>
<p>This page is used to inform website visitors regarding our policies with the collection, use, and
    disclosure of Personal Information if anyone decided to use our Service.</p>
<p>If you choose to use Service, then you agree to the collection and use of information in
    relation with this policy. The Personal Information that we collect are used for providing and
    improving the Service. We will not use or share your information with anyone except as described
    in this Privacy Policy.</p>
<p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions,
    which is accessible at our app, unless otherwise defined in this Privacy Policy.</p>

<p><strong>Information Collection and Use</strong></p>
<p>For a better experience while using our Service, we may require you to provide certain
    personally identifiable information, including but not limited to your advertising identiers, and conneting app to the web server and storing the marks for average calculation. 
	The information that we request is retained on your device and is not
    collected by me in any way and used as described in this privacy policy.</p>
<p>The app does use third party services that may collect information used to identify you. Only google services api's are used and the firebase service for the database.

<p><strong>Log Data</strong></p>
<p>We want to inform you that whenever you use [my|our] Service, in case of an error in the app we collect
    data and information (through third party products) on your phone called Log Data. This Log Data
    may include information such as your devices’s Internet Protocol (“IP”) address, device name,
    operating system version, configuration of the app when utilising our Service, the time and date
    of your use of the Service, and other statistics.</p>

<p><strong>Cookies</strong></p>
<p>Cookies are files with small amount of data that is commonly used an anonymous unique identifier.
    These are sent to your browser from the website that you visit and are stored on your devices’s
    internal memory.</p>
<p>This Services does not uses these “cookies” explicitly. However, the app may use third party code
    and libraries that use “cookies” to collection information and to improve their services. You
    have the option to either accept or refuse these cookies, and know when a cookie is being sent
    to your device. If you choose to refuse our cookies, you may not be able to use some portions of
    this Service.</p>


<p><strong>Security</strong></p>
<p>We value your trust in providing us your Personal Information, thus we are striving to use
    commercially acceptable means of protecting it. But remember that no method of transmission over
    the internet, or method of electronic storage is 100% secure and reliable, and we cannot
    guarantee its absolute security. We send your userid and password to our server through secured Https server, and we do not store any of the mentioned.</p>


<p><strong>Contact Us</strong></p>
<p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact
   us.</p>
<p>This Privacy Policy page was created at <a href="https://privacypolicytemplate.net"
                                              target="_blank">privacypolicytemplate.net</a>.</p>
</body>
</html>

